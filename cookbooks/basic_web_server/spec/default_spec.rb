require 'chefspec'
require 'chefspec/berkshelf'

describe 'basic_web_server::default' do
	before {
		stub_command("/usr/sbin/httpd -t").and_return(true)
	}

	subject {
		ChefSpec::SoloRunner.new(
			platform:'centos',
			version:'6.5'
		).converge(described_recipe)
	}

	it { should include_recipe('apache2') }
	it { should include_recipe('apache2::mod_rewrite') }

	it { should render_file('/var/www/html/index.html').with_content('Automation for the People') }

end