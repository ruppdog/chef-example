name             'basic_web_server'
maintainer       'Tyler Ruppert'
maintainer_email 'tylerruppert@gmail.com'
license          'All rights reserved'
description      'Installs/Configures basic_web_server'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.19'
depends 'apache2'
