basic_web_server CHANGELOG
==========================

This file is used to list changes made in each version of the basic_web_server cookbook.

0.1.0
-----
- Tyler Ruppert - Initial release of basic_web_server

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
