#
# Cookbook Name:: basic_web_server
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe "apache2"
include_recipe "apache2::mod_rewrite"

template '/var/www/html/index.html' do
	source 'index.html.erb'
	owner 'root'
	group 'root'
	mode '0755'
end

web_app 'basic_web_server'
